# Flexyforce React Assessment

## Requirements

Create a static React application displaying data from a [Star Wars API](https://swapi.co/) (If you are familiar with GraphQL use the [GraphQL API](https://github.com/graphql/swapi-graphql) instead for bonus points!). 

You may fork this repo into your own private repo and start from there.

* You should be able to navigate through this data in an intuitive and user friendly manner.
* You should be able to edit _some_ of the data locally. We will leave it up to you to decide which data and how to edit it. (It is _not_ necessary for these changes to persist on page reload).
* You are free to be creative in how to implement this, but always ensure a good user experience.
* Use the latest version of React and Node - A `package.json` file is included in this repository for you to use.
* Give some thought to what will make a decent user experience. We would like to see something readable but with no need to go all out on sleek and flashy UI elements.
* Use any supporting technologies, frameworks, package management, build systems and libraries that you are familiar with and feel are appropriate.
* Create your own README file that:
    1.	Documents all necessary instructions and info for your app.
    2.	Documents anything you might implement with more time (features, fixes, technical debt corrections etc).
* *Do not spend more than 4 hours on your solution* (even if you do not complete the entire exercise).
* It is understandable if the entire task is not completed, but ensure that what you do submit is _production_ quality.
* In order to save you time, we have provided a basic webpack and babel build setup and dev server for you. Feel free to update this however you see fit.
* Ensure good development practices are followed at all times.

## Bonus points
* Use local state management frameworks such as Redux, Flux, or Apollo Client.
* Use new EcmaScript features
* Use any relevant build tools or pipelines
* Host your code on a public web server of your choice.
* Impress us in any way

## What we are looking for
* A good user experience
* Maintainability
* Lightweight, performant, and robust code and data structures
* Good development practices as you would while working on a real system in production.

## Submission instructions

Push your code into your own _private_ repository of your choice and share the link with:  
hello@flexyforce.com  
murray.leroux@flexyforce.com
josh@flexyforce.com

Good luck and thank you for your time - we look forward to seeing your submissions!
